window.onload = () => {
    // 2.1 Inserta dinamicamente en un html un div vacio con javascript.
    const body = document.querySelector('body');
    const newDiv = document.createElement('div');
    body.appendChild(newDiv);
    // 2.2 Inserta dinamicamente en un html un div que contenga una p con javascript.
    const newDiv2 = document.createElement('div');
    const newP = document.createElement('p');
    newDiv2.appendChild(newP);
    body.appendChild(newDiv2);
    // 2.3 Inserta dinamicamente en un html un div que contenga 6 p utilizando un loop con javascript.
    let numDivs = 6;
    const newDiv3 = document.createElement('div');
    for (let i = 0; i < numDivs; i++) {
        const newP2 = document.createElement('p');
        newDiv3.appendChild(newP2);
    }
    body.appendChild(newDiv3);
    // 2.4 Inserta dinamicamente con javascript en un html una p con el texto 'Soy dinámico!'.
    const newP3 = document.createElement('p');
    newP3.innerText = 'Soy dinámico!';
    body.appendChild(newP3);
    // 2.5 Inserta en el h2 con la clase .fn-insert-here el texto 'Wubba Lubba dub dub'.
    const h2 = document.querySelector('.fn-insert-here');
    h2.innerText = 'Wubba Lubba dub dub';
    // 2.6 Basandote en el siguiente array crea una lista ul > li con los textos del array.
    const apps = ['Facebook', 'Netflix', 'Instagram', 'Snapchat', 'Twitter'];
    const newUl = document.createElement('ul');
    for (let i = 0; i < apps.length; i++) {
        const newLi = document.createElement('li');
        newLi.innerText = apps[i];
        newUl.appendChild(newLi);
    }
    body.appendChild(newUl);
    // 2.7 Elimina todos los nodos que tengan la clase .fn-remove-me
    const fnRemove = document.querySelectorAll('.fn-remove-me');
    fnRemove.forEach(fn => {
        fn.remove();
    });
    // 2.8 Inserta una p con el texto 'Voy en medio!' entre los dos div. Recuerda que no solo puedes insertar elementos con .appendChild.
    const div28 = document.querySelector('div');
    const newP4 = document.createElement('p');
    newP4.innerText = 'Voy en medio!';
    div28.insertAdjacentHTML('afterend', '<p>Voy en medio!</p>');
    // 2.9 Inserta p con el texto 'Voy dentro!', dentro de todos los div con la clase .fn-insert-here
    const fnInsert = document.querySelectorAll('.fn-insert-here');
    fnInsert.forEach(pes => {
        if (pes instanceof HTMLDivElement) {
            const newP5 = document.createElement('p');
            newP5.innerText = 'Voy en dentro!';
            pes.appendChild(newP5);
        }
    });
};
